//: Playground - noun: a place where people can play

import Cocoa

// need to import this to use SCDynamicStore

import SystemConfiguration

var str = "Hello, playground"

// some Strings to play with

let funnyThing = "🍌"
let somethingInSwedish = "God eftermiddag Göteborg."

// and how to get it to show up in the logs

print(funnyThing + " " + somethingInSwedish)

// quickly create an array

var array = [ 1, 2, 4, 5 ]

// loop through that array and print out each element

for num in array {
    print(num)
}

// a quick function to get the actual console user
// it takes no argument and returns a String of the user's name

public func getConsoleUser() -> String {
    var uid: uid_t = 0
    var gid: gid_t = 0
    var userName: String = ""
    
    // use SCDynamicStore to find out who the console user is
    // the in/out pointers will hold the UID and GID of the user if you need them
    
    let theResult = SCDynamicStoreCopyConsoleUser( nil, &uid, &gid)
    userName = theResult! as String
    return userName
}

print(getConsoleUser())

// how to add a new element to an array

array.append(7)

print(array)

// a function that wraps up most of the nastiness of using a shell command

func cliTask(task: String) -> String {

// first set up the process and the output and error pipes

let myTask = Process()
let myOutPipe = Pipe()
let myErrorPipe = Pipe()

// this breaks up the full shell command into the actual command then
// the variables that go with it
// NOTE: the command needs to be the full path, so /usr/bin/dig, not just dig
    
var command = task.components(separatedBy: " ")
let launchPath = command.remove(at: 0)

// now you set all the parts of the Process()

myTask.launchPath = launchPath
myTask.arguments = command
myTask.standardOutput = myOutPipe
myTask.standardError = myErrorPipe

// this actually starts the Process()

myTask.launch()

// and then wait until it exits which makes this a blocking process, which is
// probably what you want to begin with

myTask.waitUntilExit()

// get the output from the pipes and cast from data to Strings

let data = myOutPipe.fileHandleForReading.readDataToEndOfFile()
let output = String(data: data, encoding: String.Encoding.utf8)

// return the output

return output!
    
}

// and an example of how to actually use the function

var myReturn = cliTask(task: "/usr/bin/dig +short www.apple.com").components(separatedBy: "\n")

// removes the extra line at the end

myReturn.removeLast()

// prints out all of the hosts

for line in myReturn {
    print("host: " + line )
}

// gets the user's names

print(NSUserName())

// and how easy it is to make a String uppercase

print(NSFullUserName().uppercased())

let myName = NSUserName()

// and how to use switch/case 

switch myName {
    case "mactroll" : print("really cool")
    case "Brangelina" : print("No longer")
    default :   print("Shouldn't be here")
}